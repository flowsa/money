<?php
/**
 * money plugin for Craft CMS 3.x
 *
 * currency conversion
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communications
 */

namespace flowsa\money\services;

use flowsa\money\Money;

use Craft;
use craft\base\Component;

/**
 * MoneyService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Flow Communications
 * @package   Money
 * @since     0.0.1
 */
class MoneyService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     Money::$plugin->moneyService->exampleService()
     *
     * @return mixed
     */
    public function getConversion($from = 'USD', $to = 'ZAR', $amount = 1) {
        $path = Craft::$app->path->getStoragePath().'/currency/';
        $cache = $path. $from . '-' . $to;
        
        if(!is_dir($path)) {
            mkdir($path);
        }

        if(file_exists($cache) && filemtime($cache) > time() - (60 * 60)) {
            $rate = trim(file_get_contents($cache));
            return (float) $amount * (float) $rate;
        }
        
        $apikey = Money::$plugin->getSettings()->apikey;

        $exchange = 1;
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', "https://v6.exchangerate-api.com/v6/${apikey}/pair/${from}/${to}");


        if ($response->getStatusCode() == 200) {
            $json = json_decode($response->getBody(), true);
            if(array_key_exists('conversion_rate', $json)) {
                $exchange = $json['conversion_rate'];
            }
        }

        file_put_contents($cache, $exchange);
        return (float) $amount * $exchange;

    }
}
