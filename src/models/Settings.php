<?php
/**
 * money plugin for Craft CMS 3.x
 *
 * currency conversion
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communications
 */

namespace flowsa\money\models;

use flowsa\money\Money;

use Craft;
use craft\base\Model;

/**
 * Money Settings Model
 *
 * This is a model used to define the plugin's settings.
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Flow Communications
 * @package   Money
 * @since     0.0.1
 */
class Settings extends Model
{
    // Public Properties
    // =========================================================================

    /**
     * Some field model attribute
     *
     * @var string
     */
    public $apikey = '';

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['apikey', 'string'],
            ['apikey', 'default', 'value' => ''],
        ];
    }
}
