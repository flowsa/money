<?php
/**
 * money plugin for Craft CMS 3.x
 *
 * currency conversion
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communications
 */

/**
 * money en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('money', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Flow Communications
 * @package   Money
 * @since     0.0.1
 */
return [
    'money plugin loaded' => 'money plugin loaded',
];
