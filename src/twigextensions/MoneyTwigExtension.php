<?php
/**
 * money plugin for Craft CMS 3.x
 *
 * currency conversion
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communications
 */

namespace flowsa\money\twigextensions;

use flowsa\money\Money;

use Craft;

/**
 * Twig can be extended in many ways; you can add extra tags, filters, tests, operators,
 * global variables, and functions. You can even extend the parser itself with
 * node visitors.
 *
 * http://twig.sensiolabs.org/doc/advanced.html
 *
 * @author    Flow Communications
 * @package   Money
 * @since     0.0.1
 */
class MoneyTwigExtension extends \Twig_Extension
{
    // Public Methods
    // =========================================================================

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'Money';
    }

    /**
     * Returns an array of Twig filters, used in Twig templates via:
     *
     *      {{ 'something' | someFilter }}
     *
     * @return array
     */
    public function getFilters()
    {
        return [
            // new \Twig_SimpleFilter('convert', [$this, 'someInternalFunction']),
        ];
    }

    /**
     * Returns an array of Twig functions, used in Twig templates via:
     *
     *      {% set this = someFunction('something') %}
     *
    * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('convertCurrency', [$this, 'convertCurrency']),
        ];
    }

    /**
     * Our function called via Twig; it can do anything you want
     *
     * @param null $text
     *
     * @return string
     */
    public function convertCurrency($text = null, $currencies)
    {
        $finalTextArray = preg_split("/\[C\]/", $text);

        $finalText = "";


        foreach($finalTextArray as $key=>$textItem) {

            if (isset($currencies[$key])) {

            $currency = $currencies[$key];
            $currencyConvert = "{{ craft.money.convert('USD','ZAR',$currency)|currency('ZAR') }}";
            $finalText .= $textItem . $currencyConvert;

            } else {

              $finalText .= $textItem;

            }
        }

        return $finalText;
    
    }
}
