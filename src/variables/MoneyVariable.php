<?php
/**
 * money plugin for Craft CMS 3.x
 *
 * currency conversion
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communications
 */

namespace flowsa\money\variables;

use flowsa\money\Money;

use Craft;

/**
 * money Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.money }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Flow Communications
 * @package   Money
 * @since     0.0.1
 */
class MoneyVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.money.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.money.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function convert($from,$to,$amount)
    {
        $result = Money::$plugin->moneyService->getConversion($from,$to,$amount);
        return $result;
    }
}
